#!/usr/bin/env python3
"""
Author : Craig Treptow <craig.treptow@gmail.com>
Date   : 2022-12-07
Purpose: Rock the Casbah
"""

import argparse
import os


# --------------------------------------------------
def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('text',
                        metavar='text',
                        help='Some words')

    parser.add_argument('-v',
                        '--vowel',
                        help='A vowel',
                        metavar='vowel',
                        type=str,
                        default='a',
                        choices=['a', 'e', 'i', 'o', 'u'])

    args = parser.parse_args()

    if os.path.isfile(args.text):
        args.text = open(args.text).read().rstrip()

    return args


# --------------------------------------------------
def main():
    """Replace vowels with provided vowel"""

    args = get_args()
    text = args.text
    vowel = args.vowel
    lower_vowels = ['a', 'e', 'i', 'o', 'u']
    upper_vowels = ['A', 'E', 'I', 'O', 'U']

    modified_letters = []

    for letter in text:
        if letter in lower_vowels:
            modified_letters.append(vowel.lower())
        elif letter in upper_vowels:
            modified_letters.append(vowel.upper())
        else:
            modified_letters.append(letter)

    print(''.join(modified_letters))


# --------------------------------------------------
if __name__ == '__main__':
    main()
