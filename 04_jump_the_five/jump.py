#!/usr/bin/env python3
"""
Author : Craig Treptow <craig.treptow@gmail.com>
Date   : 2022-11-28
Purpose: Rock the Casbah
"""

import argparse


# --------------------------------------------------
def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('input',
                        metavar='str',
                        help='The input')

    return parser.parse_args()


# --------------------------------------------------
def main() -> str:
    """Convert numbers to fool cops on _The Wire_"""

    args = get_args()
    input = args.input
    output = ''
    translation = {'1': '9',
                   '2': '8',
                   '3': '7',
                   '4': '6',
                   '5': '0',
                   '6': '4',
                   '7': '3',
                   '8': '2',
                   '9': '1',
                   '0': '5'}

    for letter in input:
        if letter.isdigit():
            output += translation[letter]
        else:
            output += letter

    return output


# --------------------------------------------------
if __name__ == '__main__':
    print(main())
