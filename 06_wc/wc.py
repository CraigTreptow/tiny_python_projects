#!/usr/bin/env python3
"""
Author : Craig Treptow <craig.treptow@gmail.com>
Date   : 2022-11-30
Purpose: Rock the Casbah
"""

import argparse
import sys


# --------------------------------------------------
def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Emulate wc (word count)',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('file',
                        metavar='FILE',
                        nargs='*',
                        type=argparse.FileType('rt'),
                        default=[sys.stdin],
                        help='Input file(s)')

    return parser.parse_args()


# --------------------------------------------------
def main():
    """Count lines, words, and bytes from intput file(s)"""

    args = get_args()
    total_line_count, total_word_count, total_byte_count = 0, 0, 0

    for fh in args.file:
        line_count, word_count, byte_count = 0, 0, 0

        for line in fh:
            line_count += 1
            word_count += len(line.split())
            byte_count += len(line)

        total_line_count += line_count
        total_word_count += word_count
        total_byte_count += byte_count
        print(f'{line_count:8} {word_count:8} {byte_count:8} {fh.name}')

    if len(args.file) > 1:
        print(f'{total_line_count:8} {total_word_count:8} {total_byte_count:8} total')


# --------------------------------------------------
if __name__ == '__main__':
    main()
