#!/usr/bin/env python3
"""
Author : Craig Treptow <craig.treptow@gmail.com>
Date   : 2022-11-27
Purpose: Rock the Casbah
"""

import argparse


# --------------------------------------------------
def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('sighted',
                        metavar='str',
                        help='Something sighted')

    return parser.parse_args()


# --------------------------------------------------
def main() -> None:
    """Notifies the captain what the crowsnest saw"""

    args = get_args()
    thing_sighted = args.sighted
    # author's
    article = 'an' if thing_sighted[0].lower() in 'aeiou' else 'a'

    # mine
    # article = ''

    # if thing_sighted[0].lower() in 'aeiou':
    #     article = 'an'
    # else:
    #     article = 'a'

    print(f'Ahoy, Captain, {article} {thing_sighted} off the larboard bow!')


# --------------------------------------------------
if __name__ == '__main__':
    main()
