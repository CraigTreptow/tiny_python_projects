#!/usr/bin/env python3
"""
Author : "Craig Treptow" <craig.treptow@gmail.com>
Date   : 2022-12-11
Purpose: Rock the Casbah
"""

import argparse


# --------------------------------------------------
def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-n',
                        '--num',
                        help='How many bottles',
                        metavar='int',
                        type=int,
                        default=10)

    args = parser.parse_args()

    if args.num < 1:
        parser.error(
            f'--num "{args.num}" must be greater than 0')

    return args


def verse_for(number: int):
    """return verse for beer drinking game"""
    return f'{_bottles_on_wall(number)}\n{_take_one()}\n{_beers_left_on_wall(number - 1)}'


def _bottles_on_wall(number: int) -> str:
    maybe_s = 's' if number > 1 else ''

    return f'{number} bottle{maybe_s} of beer on the wall,\n{number} bottle{maybe_s} of beer,'


def _take_one() -> str:
    return 'Take one down, pass it around,'


def _beers_left_on_wall(number: int) -> str:
    if number == 0:
        return 'No more bottles of beer on the wall!'

    maybe_s = '' if number == 1 else 's'
    return f'{number} bottle{maybe_s} of beer on the wall!\n'


# --------------------------------------------------
def main():
    """Return the beer drinking song verses"""

    args = get_args()
    number = args.num

    for i in range(number, 0, -1):
        print(verse_for(i))


# --------------------------------------------------
if __name__ == '__main__':
    main()
