# tiny_python_projects

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/CraigTreptow/tiny_python_projects)

<a href="https://gitpod.io/#https://gitlab.com/CraigTreptow/tiny_python_projects">
  <img
    src="https://img.shields.io/badge/Contribute%20with-Gitpod-908a85?logo=gitpod"
    alt="Contribute with Gitpod"
  />
</a>

## Introduction

This is my implementation of the problems in the book [Tiny Python Projects](https://tinypythonprojects.com/)

## Progress

- [X] 01_hello
- [X] 02_crowsnest
- [X] 03_picnic
- [X] 04_jump_the_five
- [X] 05_howler
- [X] 06_wc
- [X] 07_gashlycrumb
- [X] 08_apples_and_bananas
- [X] 09_abuse
- [X] 10_telephone
- [X] 11_bottles_of_beer
- [X] 12_ransom
- [X] 13_twelve_days
- [X] 14_rhymer
- [X] 15_kentucky_friar
- [X] 16_scrambler19_wod
- [X] 17_mad_libs
- [ ] 18_gematria
- [ ] 19_wod
- [ ] 20_password
- [ ] 21_tictactoe
- [ ] 22_itictactoe

## Setup

`python3 -m pip install -r requirements.txt`

## Testing

There is a directory for each chapter of the book.
Each directory contains a `test.py` program you can use with `pytest` to check that you have written the program correctly.
I have included a short README to describe each exercise.

### Run all tests

At the top level, we have one option:
- run `/.all_test.sh`

### Run tests for a single project

In a project directory, we have a couple of options:
- run `pytest -v test.py`
- run `make`
