#!/usr/bin/env python3
"""
Author : Craig Treptow <craig.treptow@gmail.com>
Date   : 2022-12-04
Purpose: Rock the Casbah
"""

import argparse


# --------------------------------------------------
def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('letters',
                        metavar='letter',
                        nargs='+',
                        help='Letters')

    parser.add_argument('-f',
                        '--file',
                        help='A readable file',
                        metavar='FILE',
                        type=argparse.FileType('rt'),
                        default='./gashlycrumb.txt')

    return parser.parse_args()


# --------------------------------------------------
def main():
    """Pull lines matching the given letter from gashlycrumb.txt by default"""

    args = get_args()
    letters = args.letters
    file = args.file
    letter_map = dict()

    for line in file:
        letter_map[line[0]] = line.rstrip()

    for letter in letters:
        if letter.upper() in letter_map:
            print(letter_map[letter.upper()])
        else:
            print(f'I do not know "{letter}".')


# --------------------------------------------------
if __name__ == '__main__':
    main()
