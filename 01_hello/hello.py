#!/usr/bin/env python3
"""
Author: Craig Treptow
Purpose: Say hello
"""

import argparse


def get_args():
    """Get command-line arguments"""
    parser = argparse.ArgumentParser(description='Say hello')
    # -- makes the parameter optional
    parser.add_argument('-n', '--name', metavar='name',
                        default='World', help='Name to greet')
    return parser.parse_args()


def main() -> None:
    """Get arguments from command line and say hello"""
    args = get_args()

    print('Hello, ' + args.name + '!')


if __name__ == '__main__':
    main()
