#!/usr/bin/env python3
"""
Author : Craig Treptow <craig.treptow@gmail.com>
Date   : 2022-11-27
Purpose: Rock the Casbah
"""

import argparse


# --------------------------------------------------
def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='nargs=+',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('items',
                        metavar='str',
                        nargs='+',
                        type=str,
                        help='items for a picnic')

    parser.add_argument('-s',
                        '--sorted',
                        help='sort the provided items',
                        action='store_true')

    return parser.parse_args()


# --------------------------------------------------
def main():
    """Make a jazz noise here"""

    args = get_args()
    items = args.items
    items_length = len(items)
    sorted = args.sorted

    if sorted:
        items.sort()

    if items_length == 1:
        print(f"You are bringing {items[0]}.")
    elif items_length == 2:
        print(f"You are bringing {items[0]} and {items[1]}.")
    else:
        msg = 'You are bringing '

        for item in items[0:-1]:
            msg = msg + f"{item}, "

        msg = msg + f"and {items[-1]}."

        print(msg)


# --------------------------------------------------
if __name__ == '__main__':
    main()
