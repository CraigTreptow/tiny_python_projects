#!/usr/bin/env python3
"""
Author : "Craig Treptow" <craig.treptow@gmail.com>
Date   : 2022-12-08
Purpose: Rock the Casbah
"""

import argparse
import random


# --------------------------------------------------
def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-a',
                        '--adjectives',
                        help='Number of adjectives',
                        metavar='number',
                        type=int,
                        default=2)

    parser.add_argument('-n',
                        '--number',
                        help='Number of insults',
                        metavar='number',
                        type=int,
                        default=3)

    parser.add_argument('-s',
                        '--seed',
                        help='Random seed',
                        metavar='number',
                        type=int,
                        default=None)

    args = parser.parse_args()

    if args.adjectives <= 0:
        parser.error(f'--adjectives "{args.adjectives}" must be > 0')

    if args.number < 0:
        parser.error(f'--number "{args.number}" must be > 0')

    if args.seed < 0:
        parser.error(f'--seed "{args.seed}" must be > 0')

    return args


# --------------------------------------------------
def main() -> None:
    """Make a jazz noise here"""

    adjectives = """
      bankrupt base caterwauling corrupt cullionly detestable dishonest false
      filthsome filthy foolish foul gross heedless indistinguishable infected insatiate
      irksome lascivious lecherous loathsome lubbery old peevish rascaly rotten
      ruinous scurilous scurvy slanderous sodden-witted thin-faced toad-spotted
      unmannered vile wall-eyed
    """.split()

    nouns = """
      Judas Satan ape ass barbermonger beggar block boy braggart butt carbuncle
      coward coxcomb cur dandy degenerate fiend fishmonger fool gull harpy jack
      jolthead knave liar lunatic maw milksop minion ratcatcher recreant rogue scold
      slave swine traitor varlet villain worm
    """.split()

    args = get_args()
    random.seed(args.seed)

    adjectives_required = args.adjectives
    insults_required = args.number

    for _n in range(insults_required):
        chosen_adjectives = ", ".join(
            random.sample(adjectives, k=adjectives_required))
        print(f"You {chosen_adjectives} {random.choice(nouns)}!")


# --------------------------------------------------
if __name__ == '__main__':
    main()
