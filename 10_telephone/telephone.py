#!/usr/bin/env python3
"""
Author : "Craig Treptow" <craig.treptow@gmail.com>
Date   : 2022-12-10
Purpose: Rock the Casbah
"""

import argparse
import os
import random
import string


# --------------------------------------------------
def get_args() -> argparse.Namespace:
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('text',
                        metavar='text',
                        type=str,
                        help='A string or path and file name')

    parser.add_argument('-m',
                        '--mutations',
                        help='The percentage of letters to be mutated. (0.0 - 1.0)',
                        metavar='float',
                        type=float,
                        default=0.1)

    parser.add_argument('-s',
                        '--seed',
                        help='Random seed',
                        metavar='number',
                        type=int,
                        default=None)

    args = parser.parse_args()

    if args.mutations < 0 or args.mutations > 1:
        parser.error(
            f'--mutations "{args.mutations}" must be between 0 and 1')

    if os.path.isfile(args.text):
        with open(args.text, 'rt') as f:
            args.text = f.read().rstrip()

    return args


# --------------------------------------------------
def main() -> None:
    """Recreate the 'telephone' game from elementary school"""

    args = get_args()
    text = args.text
    random.seed(args.seed)
    mutation_percentage = args.mutations
    mutation_count = round(len(text) * mutation_percentage)
    alpha = ''.join(sorted(string.ascii_letters + string.punctuation))
    new_text = text

    for i in random.sample(range(len(text)), mutation_count):
        new_char = random.choice(alpha.replace(new_text[i], ''))
        new_text = new_text[:i] + new_char + new_text[i + 1:]

    print(f'You said: "{text}"\nI heard : "{new_text}"')


# --------------------------------------------------
if __name__ == '__main__':
    main()
