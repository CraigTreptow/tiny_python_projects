#!/usr/bin/env python3
"""
Author : "Craig Treptow" <craig.treptow@gmail.com>
Date   : 2022-12-11
Purpose: Rock the Casbah
"""

import argparse
import os
import random


# --------------------------------------------------
def get_args() -> argparse.Namespace:
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('text',
                        metavar='text',
                        type=str,
                        help='A string or path and file name')

    parser.add_argument('-s',
                        '--seed',
                        help='Random seed',
                        metavar='number',
                        type=int,
                        default=None)

    args = parser.parse_args()

    if os.path.isfile(args.text):
        with open(args.text, 'rt') as f:
            args.text = f.read().rstrip()

    return args


# --------------------------------------------------
def main() -> None:
    """Create a ransom letter style message from given input"""
    # import pdb
    # pdb.set_trace()

    args = get_args()
    text = args.text
    random.seed(args.seed)

    new_words = [letter.upper() if random.choice([False, True])
                 else letter.lower() for letter in text]
    print(''.join(new_words))


# --------------------------------------------------
if __name__ == '__main__':
    main()
