#!/usr/bin/env python3
"""
Author : "Craig Treptow" <craig.treptow@gmail.com>
Date   : 2022-12-15
Purpose: Rock the Casbah
"""

import argparse
import os
import random
import re


# --------------------------------------------------
def get_args() -> argparse.Namespace:
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('text',
                        metavar='text',
                        type=str,
                        help='A string or path and file name')

    parser.add_argument('-s',
                        '--seed',
                        help='Random seed',
                        metavar='number',
                        type=int,
                        default=None)

    args = parser.parse_args()

    if os.path.isfile(args.text):
        with open(args.text, 'rt') as f:
            args.text = f.read().rstrip()

    return args


def scramble(word: str) -> str:
    if len(word) <= 3:
        return word
    else:
        first_letter = word[0]
        middle = list(word[1:-1])
        random.shuffle(middle)
        last_letter = word[-1]
        return first_letter + ''.join(middle) + last_letter

# --------------------------------------------------


def main() -> None:
    """Randomize the middle of the given words"""
    # import pdb
    # pdb.set_trace()

    args = get_args()
    text = args.text
    random.seed(args.seed)

    #                                ?: is non-capturing
    #                                used with optionals  ?
    splitter = re.compile("([a-zA-Z](?:[a-zA-Z']*[a-zA-Z])?)")

    for line in text.splitlines():
        print(''.join(map(scramble, splitter.split(line))))


# --------------------------------------------------
if __name__ == '__main__':
    main()
