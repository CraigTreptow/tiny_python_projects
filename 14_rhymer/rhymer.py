#!/usr/bin/env python3
"""
Author : "Craig Treptow" <craig.treptow@gmail.com>
Date   : 2022-12-12
Purpose: Rock the Casbah
"""

import argparse
import re
import string as s


# --------------------------------------------------
def get_args() -> argparse.Namespace:
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Make rhyming words',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('word',
                        metavar='word',
                        help='A word to rhyme')

    return parser.parse_args()


def stemmer(word: str) -> tuple[None | str, None | str]:
    """Return the stem of a word"""

    word = word.lower()
    consonants = ''.join([c for c in s.ascii_lowercase if c not in 'aeiou'])
    vowels = 'aeiou'
    pattern = (
        '([' + consonants + ']+)?'  # capture one or more consonants, optional
        '([aeiou])'                # capture at least one vowel
        '(.*)'                     # capture zero or more of anything
    )

    match = re.match(pattern, word, re.IGNORECASE)

    if match:
        consonants = match.group(1) or ''
        vowel = match.group(2) or ''
        the_rest = match.group(3) or ''
        return (consonants, vowel + the_rest)
    else:
        return (word, '')

# --------------------------------------------------


def main() -> None:
    """Make a jazz noise here"""

    args = get_args()
    word = args.word

    prefixes = list('bcdfghjklmnpqrstvwxyz') + ('bl br ch cl cr dr fl fr gl gr pl pr sc '
                                                'sh sk sl sm sn sp st sw th tr tw thw wh wr '
                                                'sch scr shr sph spl spr squ str thr').split()

    start, rest = stemmer(word)

    if rest:
        print('\n'.join(sorted([p + rest for p in prefixes if p != start])))
    else:
        print(f'Cannot rhyme "{args.word}"')


# --------------------------------------------------
if __name__ == '__main__':
    main()
