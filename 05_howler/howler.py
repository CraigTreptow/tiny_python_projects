#!/usr/bin/env python3
"""
Author : Craig Treptow <craig.treptow@gmail.com>
Date   : 2022-11-28
Purpose: Rock the Casbah
"""

import argparse
import os


# --------------------------------------------------
def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Rock the Casbah',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('input',
                        metavar='str',
                        help='A string or path and file name')

    parser.add_argument('-o',
                        '--outfile',
                        help='Output file',
                        metavar='FILE',
                        type=str,
                        default='')

    return parser.parse_args()


# --------------------------------------------------
def main() -> None:
    """Howl, like in Harry Potter"""

    args = get_args()
    input = args.input
    outfile = args.outfile

    str = ""
    if os.path.isfile(input):
        with open(input, 'rt') as f:
            str = f.read().rstrip().upper()
    else:
        str = input.upper()

    if outfile:
        with open(outfile, 'wt') as f:
            f.write(str)
    else:
        print(str)


# --------------------------------------------------
if __name__ == '__main__':
    main()
